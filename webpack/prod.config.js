const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const baseConfig = require('./base.config.js');

const CompressionPlugin = require("compression-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");


//Основные настройки
module.exports = merge(baseConfig, {
    entry: ["babel-polyfill", "./frontend/vue/main.js"],
    output: {
        path: path.resolve(__dirname, '../static/site'),
        publicPath: '/static/site/',
        filename: 'build.js'
    },
    performance: {
        hints: false
    },
    devtool: '#eval-source-map'
});


//Плагины
module.exports.devtool = '#source-map';
// http://vue-loader.vuejs.org/en/workflow/production.html
module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: '"production"'
        }
    }),
    new webpack.optimize.CommonsChunkPlugin({
        children: true,
        async: true,
    }),
    new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        comments: false,
        compress: {
            sequences     : true,
            booleans      : true,
            loops         : true,
            unused      : true,
            warnings    : false,
            drop_console: true,
            unsafe      : true
        }
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.LoaderOptionsPlugin({
        minimize: true
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new ExtractTextPlugin("css/styles.css"),
    new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
    })
]);