const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


if (process.env.NODE_ENV === 'production') {
    module.exports = {
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                },
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.sass$/,
                    use: ExtractTextPlugin.extract({
                        use: [{
                            loader: "css-loader"
                        }, {
                            loader: "sass-loader"
                        }],
                        // use style-loader in development
                        fallback: "style-loader"
                    }),
                },
                {
                    test: /\.css/,
                    loader: 'style-loader!css-loader'
                },
                {
                    test: /\.(gif|png|jpe?g|ico)$/i,       /*Лоадер картинок*/
                    loaders: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'images/[name].[ext]'
                            }
                        },
                        {
                            loader: 'image-webpack-loader',
                            options: {
                                gifsicle: {
                                    interlaced: false,
                                },
                                optipng: {
                                    optimizationLevel: 7,
                                },
                                pngquant: {
                                    quality: '65-90',
                                    speed: 4
                                },
                                mozjpeg: {
                                    progressive: true,
                                    quality: 65
                                },
                            }
                        },
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|svg)$/,     /*Лоадер-шрифтов*/
                    exclude: /node_modules/,
                    loader: 'url-loader?limit=1024&name=fonts/[name].[ext]'
                },
            ]
        }
    };
}

else if (process.env.NODE_ENV === 'development') {
    module.exports = {
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                },
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.sass$/,

                    use: [{
                        loader: 'style-loader'
                    },{
                        loader: "css-loader", options: {
                            sourceMap: true
                        }
                    }, {
                        loader: "sass-loader", options: {
                            sourceMap: true
                        }
                    }],
                },
                {
                    test: /\.css/,
                    loader: 'style-loader!css-loader'
                },
                {
                    test: /\.(gif|png|jpe?g|ico)$/i,       /*Лоадер картинок*/
                    loaders: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'images/[name].[ext]'
                            }
                        },
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|svg)$/,     /*Лоадер-шрифтов*/
                    exclude: /node_modules/,
                    loader: 'url-loader?limit=1024&name=fonts/[name].[ext]'
                },
            ]
        }
    };
}

