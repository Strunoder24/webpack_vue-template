import Vue from 'vue';
import VeeValidate from 'vee-validate';


import { Validator } from 'vee-validate';
const dictionary = {
    ru: {
        custom:{
            url: {
                regex: 'Можно использовать только буквы английского алфавита, цифры, символы подчеркивания или дефис, при этом значение не может состоять из одних лишь цифр',
                required: 'Это поле не может быть пустым!'
            },

            name: {
                required: 'Это поле не может быть пустым!'
            },
            firstName: {
                required: 'Это поле не может быть пустым!'
            },
            lastName: {
                required: 'Это поле не может быть пустым!'
            },
            email: {
                required: 'Это поле не может быть пустым!',
                email: 'Вы указали неверный email'
            },
            changePassword: {
                required: 'Это поле не может быть пустым!',
                confirmed: 'Пароли не совпадают'
            },
            login: {
                required: 'Это поле не может быть пустым!'
            },
            password: {
                required: 'Это поле не может быть пустым!'
            },

        }
    }
};

Validator.updateDictionary(dictionary);

Vue.use(VeeValidate, {
    locale: 'ru',
    events: 'blur|input'
});
