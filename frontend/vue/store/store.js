import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import asyncRequestsHandler from './modules/asyncRequestsHandler';
import * as getters from './getters';
import * as mutations from './mutations';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
    },
    getters,
    mutations,
    modules: {
        // asyncRequestsHandler,
    },
    plugins: [
        createPersistedState({
            paths:[
                // 'project_id',                             /*Примеры путей*/
                // 'ProjectPageStore.DraftPageContent',
                // 'ProjectPageStore.currentPageId',
                // 'ProjectPageStore.navBarOpenStatus'
            ]
        })
    ]
});