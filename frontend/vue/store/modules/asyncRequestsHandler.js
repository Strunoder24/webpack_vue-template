const state = {
//  страницы
    TreeListStatus: '',
};

const getters = {
    getTreeList: state => {
        return state.TreeListStatus
    },
};

const mutations = {
    setTreeList: (state, payload) => {
        state.TreeListStatus = payload
    },
};

export default {
    state,
    getters,
    mutations,
}