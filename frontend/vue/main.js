import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';

// рекваирменты для вебпака. Чтобы он мог их собрать
import '../src/sass/main.sass';    /*Импортирую sass для прод сборки вебпаком*/

import './config/validator';
import './config/moment';
import './config/axiosConf';


import {MediaQueries} from 'vue-media-queries';

import { store } from './store/store';
import { routes } from './routes';

const mediaQueries = new MediaQueries();

// Vue.prototype.moment = moment;


Vue.use(mediaQueries);
Vue.use(VueRouter);

const router = new VueRouter({
    routes,
    mode: 'history',
});

new Vue({
    el: '#app',
    store,
    router,
    mediaQueries: mediaQueries,
    render: h => h(App),
});

export default {
    router
}
